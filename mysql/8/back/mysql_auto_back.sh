#!/bin/bash
#########数据库基础信息#############
DB_HOST=127.0.0.1
DB_NAME=test
DB_USER=root
DB_PASS=111111
DATE=`date +%Y%m%d%H%M%S`
MYSQL_PATH=./
#保存备份个数
BACK_NUM=100

##########################
back_up(){

    #bakup file absolute path
    BAKUP_FILE=${MYSQL_PATH}${DB_NAME}${DATE}.sql

    #执行mysqldump命令的参数
    SQL_OPT="-u${DB_USER} -p${DB_PASS} -h ${DB_HOST} ${DB_NAME}"

    #执行备份，整个DB所有表的备份，SQL语句的形式
    mysqldump ${SQL_OPT} > ${BAKUP_FILE}

    # 删除30天之前的就备份文件
    #find ${MYSQL_PATH}/* -mtime +10 -exec rm {} \;

    #写创建备份日志
    echo "create ${BAKUP_FILE}"  >> ${MYSQL_PATH}log.txt

    #找出需要删除的备份
    delfile=`ls -l -crt  ${MYSQL_PATH}$1*.sql | awk '{print $9 }' | head -1`
    #echo "${delfile}"

    #判断现在的备份数量是否大于$number
    count=`ls -l -crt  ${MYSQL_PATH}$1*.sql | awk '{print $9 }' | wc -l`
    if [ $count -gt $BACK_NUM ];then
        #删除最早生成的备份，只保留number数量的备份
        rm $delfile
        #写删除文件日志
        echo "delete $delfile" >> ${MYSQL_PATH}log.txt
    fi
}
back_up;
#############脚本结束##############
